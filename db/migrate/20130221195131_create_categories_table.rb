class CreateCategoriesTable < ActiveRecord::Migration
  def change
    create_table(:categories) do |t|
      t.string    :label, null: false, default: "", limit: 500
      
      t.timestamps
    end
    
    add_column :listings, :category_id, :integer, after: :title, default: 0, null: false
  end
end
