class Category < ActiveRecord::Base
  attr_accessible :label
  
  belongs_to :listing
end
